totalimpar=0
totalpar=0
maxn=`cat numeros.txt | wc -l`
n=1
n2=0
n3=1

echo "Números pares"
echo "_____________"

while [ $n -le $maxn ]; do
    n2=`cat numeros.txt | awk '{print $1}' | head -$n | tail -1`
    resto=`expr $n2 % 2`
    if [ $resto -eq 0 ]; then
	echo "$n2"
	totalpar=$(($totalpar+1))
    fi
    n=$(($n+1))
done

echo "TOTAL DE PARES: $totalpar"

echo "-------------------------"
echo "--------------------------"

echo "Números impares"
echo "_______________"

while [ $n3 -le $maxn ]; do 
    n2=`cat numeros.txt | awk '{print $1}' | head -$n3 | tail -1`
    resto=`expr $n2 % 2`
    if [ $resto -ne 0 ]; then
	echo "$n2"
	totalimpar=$(($totalimpar+1))
    fi
    n3=$(($n3+1))
done

echo "TOTAL IMPAR: $totalimpar"