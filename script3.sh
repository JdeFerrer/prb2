dias=1
maxd=`cat precipitaciones.txt | wc -l`

while [ $dia -le $maxd ]; do
    if [ `cat precipitaciones.txt | awk '{print $2}' | head -$dias | tail -1`-le 0 ]; then 
	dia=`cat precipitaciones.txt | awk '{print $1}' | head -$dias | tail -1`
	case $dia in
	    1) 
		echo "EL lunes 1 no hubo precipitación"
		;;
	    2)
		echo "El martes 2 no hubo precipitación"
		;;
	    3) 
		echo "El miércoles 3 no hubo precipitacion"
		;;
	    4) 
		echo "EL jueves 4 no hubo precipitación"
		;;
	    5)
		echo "El viernes 5 no hubo precipitación"
		;;
	    6) 
		echo "El sábado 6 no hubo precipitacion"
		;;
	    7) 
		echo "EL domingo 7 no hubo precipitación"
		;;
	    8)
		echo "El lunes 8 no hubo precipitación"
		;;
	    9) 
		echo "El martes 9 no hubo precipitacion"
		;;
	    10)
		echo "El miércoles día 10 no hubo precipitación"
		;;
	    esac
	fi
    dias=$(($dias+1))
done