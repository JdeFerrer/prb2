if [ -d $1 ]; then 
    cantidad=`ls $1 | grep .txt | wc -l`
    echo "Cantidad: $cantidad"
    echo "Se han borrado los $cantidad de ficheros .txt"
else
    echo "El directorio que ha introducido no existe"
fi