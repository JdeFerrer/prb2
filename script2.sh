dias=`cat precipitaciones.txt | wc -l`
x=1
y=0

while [ $x -le $dias ]; do
    acumulador=`cat precipitaciones.txt | awk '{print $2}' | head -$x |tail -1`
    y=`expr $acumulador + $y`
    x=$(($x+1))
done

media=`calc $y/$dias`

echo "La media de precipitaciones es de:$media litros"
